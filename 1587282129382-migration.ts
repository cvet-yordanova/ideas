import {MigrationInterface, QueryRunner} from "typeorm";

export class migration1587282129382 implements MigrationInterface {
    name = 'migration1587282129382'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "idea_entity" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "created" TIMESTAMP NOT NULL DEFAULT now(), "idea" text NOT NULL, "description" text NOT NULL, CONSTRAINT "PK_69834fef6d40cebb55f21c2e6db" PRIMARY KEY ("id"))`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "idea_entity"`, undefined);
    }

}
