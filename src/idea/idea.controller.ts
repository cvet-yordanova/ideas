import { Controller, Post, Get, Put, Delete, Body, Param, UsePipes, Logger, UseGuards, Options } from '@nestjs/common';
import { IdeaService } from './idea.service';
import { IdeaDTO } from './idea.dto';
import { ValidationPipe } from 'src/shared/validation.pipe';
import { AuthGuard } from 'src/auth.guard';
import { User } from 'src/user/user.decorator';
import { UserEntity } from 'src/user/user.entity';

@Controller('api/idea')
export class IdeaController {

    private logger = new Logger('IdeaController')

    constructor(private ideaService: IdeaService) {

    }

    private logData(options: any) {
        options.user && this.logger.log('USER ' + JSON.stringify(options.user));
        options.body && this.logger.log('DATA ' + JSON.stringify(options.body));
        options.id && this.logger.log('IDEA ' + JSON.stringify(options.id));
    }

    @Get()
    showAllIdeas() {
        return this.ideaService.showAll();
    }

    @Post()
    @UseGuards(new AuthGuard())
    @UsePipes(new ValidationPipe())
    createIdea(@User('id') userId: string, @Body() data: IdeaDTO) {
        this.logData({ userId, data })
        return this.ideaService.create(userId, data)
    }

    @Get(':id')
    readIdea(@Param('id') id: string) {
        return this.ideaService.read(id);
    }

    @Put(':id')
    @UseGuards(new AuthGuard())
    @UsePipes(new ValidationPipe())
    updateIdea(@Param('id') id: string, @User('id') userId: string, @Body() data: Partial<IdeaDTO>) {
        this.logData({ id, data, userId });
        return this.ideaService.update(id, userId, data)
    }

    @Delete(':id')
    @UseGuards(new AuthGuard())
    deleteIdea(@Param('id') id: string, @User('id') userId: string) {
        this.logData({ id, userId });
        return this.ideaService.destroy(id, userId);
    }

}
